﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotor : MonoBehaviour
{
    //private CharacterController controller;
    private Vector3 moveVector;

    private float verticalVelocity = 0f;
    private float speed = 4f;
    private float timeSceneStarted;
    private float gravity = -12f;

    private float animationDuration = 3f;
    private float rotation = 45f;

    private bool isDead = false;

    public Rigidbody rb;
    public GameObject cube;
    public GameObject triangle;
    public GameObject house;
    public GameObject cylinder;
    public ParticleSystem CubeTransfromParticles;
    public ParticleSystem HouseTransfromParticles;
    public ParticleSystem TriangleTransfromParticles;
    public ParticleSystem CylinderTransfromParticles;
    public AudioSource GameOver;
    public AudioSource Ambient;
    public AudioSource Transform;
    public AudioSource Rotate;
    void Start()  // Start is called before the first frame update
    {
        rb.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
        Ambient.Play();
        rb = GetComponent<Rigidbody>();
        triangle.gameObject.SetActive(false);
        house.gameObject.SetActive(false);
        cylinder.gameObject.SetActive(false);

        timeSceneStarted = Time.time;
        //controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.A))
        {
            cube.gameObject.SetActive(true);
            triangle.gameObject.SetActive(false);
            house.gameObject.SetActive(false);
            cylinder.gameObject.SetActive(false);
            CubeTransfromParticles.Play();
            Transform.Play();
            //print("cube");
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            cube.gameObject.SetActive(false);
            triangle.gameObject.SetActive(true);
            house.gameObject.SetActive(false);
            cylinder.gameObject.SetActive(false);
            TriangleTransfromParticles.Play();
            Transform.Play();
            //print("triangle");
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            cube.gameObject.SetActive(false);
            triangle.gameObject.SetActive(false);
            house.gameObject.SetActive(true);
            cylinder.gameObject.SetActive(false);
            HouseTransfromParticles.Play();
            Transform.Play();

            //print("house");
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            cube.gameObject.SetActive(false);
            triangle.gameObject.SetActive(false);
            house.gameObject.SetActive(false);
            cylinder.gameObject.SetActive(true);
            CylinderTransfromParticles.Play();
            Transform.Play();
            //print("cylinder");
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            transform.Rotate(0, rotation, 0);
            Rotate.Play();
            //print("rotate");
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            transform.Rotate(0, -rotation, 0);
            Rotate.Play();
            //print("rotate");
        }

        if(rb.transform.position.x > 2.3)
        {
            rb.constraints = RigidbodyConstraints.None;
            rb.useGravity = true;
            //print("fall");
        }

        if(rb.transform.position.x < -2.3)
        {
            rb.constraints = RigidbodyConstraints.None;
            rb.useGravity = true;

            //print("fall");
        }



        if (rb.transform.position.y < -1 && !isDead)
        {
            
            GetComponent<Score>().OnDeath();
            GameOver.Play();
            Ambient.Stop();
            isDead = true;

        }

        if (rb.transform.position.y < -1.1)
        {
            rb.useGravity = false;
            print(rb.transform.position.y);
        }
            
        
        if (isDead)
        {
            return;
        }

    
        if (Time.time < timeSceneStarted + animationDuration)
        {

            //rb.velocity = new Vector3(speed, rb.velocity.y, speed);
            return;
           
        }
 
 
        float mH = Input.GetAxis("Horizontal");
        rb.velocity = new Vector3(mH * speed, rb.velocity.y, speed);
        //moveVector = Vector3.zero;
        /*
        if (controller.isGrounded)
        {
            verticalVelocity = -0f;

        }
        else
        {
            verticalVelocity = -6f;

        }
        */

        //x - left and right
        //rb.AddForce Input.GetAxisRaw("Horizontal") * speed;
        //y - up and down
        //moveVector.y = 0f;
        //z- foward and backward
        //moveVector.z = speed;

        //rb.AddForce(moveVector * Time.deltaTime);
    }

    public void SetSpeed(float modifier)
    {
        speed = 2f + modifier;
    }
    
    void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Enemy")
        {
            Death();
            GameOver.Play();
            Ambient.Stop();
            
        }
        //print("Hit Something");

    }

    //It is being called everytime the collider hits something
    /*
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.point.z > transform.position.z + 0.3f && hit.gameObject.tag == "Enemy")
            Death();
    }
    */
    private void Death()
    {
        
        isDead = true;
        GetComponent<Score>().OnDeath();
    }

}
