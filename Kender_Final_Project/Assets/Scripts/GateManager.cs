﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GateManager : MonoBehaviour
{
    public GameObject[] gatePrefabs;

    private Transform playerTransform;
    private float spawnZ = -5f;
    private float gateLength = 20f;
    private int amnGatesOnScreen = 7;
    private float safeZone = 20f;
    private int lastPrefabIndex = 0;

    private List<GameObject> activeGates;

    // Start is called before the first frame update
    private void Start()
    {
        activeGates = new List<GameObject>();
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

        for (int i = 0; i < amnGatesOnScreen; i++)
        {
            SpawnGate();
            //print("Endless Runner");
        }

    }

    // Update is called once per frame
    private void Update()
    {
        if (playerTransform.position.z - safeZone > (spawnZ - amnGatesOnScreen * gateLength))
        {
            SpawnGate();
            DeleteGate();
        }
    }

    private void SpawnGate(int prefabIndex = -1)
    {
        GameObject go;
        go = Instantiate(gatePrefabs[RandomPrefabIndex()]) as GameObject;
        go.transform.SetParent(transform);
        go.transform.position = Vector3.forward * spawnZ;
        spawnZ += gateLength;
        activeGates.Add(go);
    }

    private void DeleteGate()
    {
        Destroy(activeGates[0]);
        activeGates.RemoveAt(0);

    }

    private int RandomPrefabIndex()
    {
        if (gatePrefabs.Length <= 1)
            return 0;

        int randomIndex = lastPrefabIndex;
        while(randomIndex == lastPrefabIndex)
        {
            randomIndex = Random.Range(0, gatePrefabs.Length);

        }

        lastPrefabIndex = randomIndex;
        return randomIndex;
    }


}
