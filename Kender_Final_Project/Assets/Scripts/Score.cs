﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{

    private float score = 0f;

    private int difficultyLevel = 1;
    private int maxDifficultylevel = 20;
    private int scoreToNextLevel = 10;

    private bool isDead = false;
    private bool HighscoreSet = false;

    public Text scoreText;
    public DeathMenu deathMenu;
    public Text DifficultyLevel;
    public Image NextSpeedLevel;
    public AudioSource SpeedUp;
    public AudioSource Highscore;
    public ParticleSystem HighscorePart;
    // Start is called before the first frame update
    void Start()
    {
        DifficultyLevel.text = 1.ToString();
    

        
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead)
        {
            return;
        }
        if (score >= scoreToNextLevel)
            LevelUp ();

        score += Time.deltaTime * difficultyLevel; 
        scoreText.text = ((int)score).ToString();

        NextSpeedLevel.fillAmount = score / scoreToNextLevel;

        if (PlayerPrefs.GetFloat("Highscore") < score && !HighscoreSet && !isDead)
        {
            HighscorePart.Play();
            Highscore.Play();
            HighscoreSet = true;


        }

    }

    void LevelUp()
    { 
        if (difficultyLevel == maxDifficultylevel)
            return;

        scoreToNextLevel *= 3;
        difficultyLevel++;
        DifficultyLevel.text = ((int)difficultyLevel).ToString();
        SpeedUp.Play();
        print(difficultyLevel);
        GetComponent<PlayerMotor>().SetSpeed(difficultyLevel);
    }

    public void OnDeath()
    {
        isDead = true;
        if (PlayerPrefs.GetFloat("Highscore") < score)
            PlayerPrefs.SetFloat("Highscore", score);

        deathMenu.ToggleEndMenu(score);
       

    }


}
